/* global QUnit */

sap.ui.require(["zui5001/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
