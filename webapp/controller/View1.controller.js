sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageBox"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, JSONModel, MessageBox) {
        "use strict";

        return Controller.extend("zui5001.controller.View1", {
            onInit: function (oEvent) {
                this.setExpression();
                this.readData();

            },

            readData() {
                var sUrl = "";
                if (window.location.href.search("launchpad") > 0) {
                    var oLength = window.location.href.length;
                    oLength = oLength - 2;
                    var sObjectId = window.location.href.substr(oLength, 2);
                    /*      this.getView().byId("parameterId").setValue(sObjectId); */
                    if (window.location.href.search("parameter") > 0) {
                        sUrl = "https://2245f5c6trial-dev-zcap001-srv.cfapps.us10.hana.ondemand.com/view-zcap001/ZTHDB002_SRV?$filter=M_ID%20eq%20%27" + sObjectId + "%27";
                    } else {
                        sUrl = "https://2245f5c6trial-dev-zcap001-srv.cfapps.us10.hana.ondemand.com/view-zcap001/ZTHDB002_SRV?$filter=M_ID%20eq%20%27C2%27";
                    }

                } else {
                    sUrl = "/view-zcap001/ZTHDB002_SRV?$filter=M_ID%20eq%20%27C2%27";
                }

                $.ajax({
                    method: "GET",
                    url: sUrl,
                    Content: "application/json",
                    dataType: "json",
                    /*                headers: {
               
                                       'Access-Control-Allow-Origin': '*'
               
                                   }, */
                    success: this.successREST.bind(this),
                    error: [this.errorREST, this]
                });
            },
            successREST: function (data, status, response) {
                var oModel = this.getView().getModel();
                var oModel = new JSONModel();
                oModel.setData({
                    "L_ID": data.value[0].L_ID,
                    "M_ID": data.value[0].M_ID,
                    "DESCRIPTION_ZF": data.value[0].DESCRIPTION_ZF,
                    "DESCRIPTION_EN": data.value[0].DESCRIPTION_EN,
                    "FORMAT": data.value[0].FORMAT,
                    "ACTIVE": data.value[0].ACTIVE
                });
                this.getView().setModel(oModel, "cData");
            },
            errorREST: function (error) {
            },
            setExpression() {
                var inputModel = new JSONModel({
                    "expression": ""
                });
                this.getView().setModel(inputModel, "inputModel");
            },
            onClearInput: function () {
                this.getView().getModel("inputModel").setProperty("/expression", "");
            },
            editInput: function (ch) {
                var expression = this.getView().getModel("inputModel").getProperty("/expression");
                if ("+/*-".indexOf(ch) == -1) {
                    expression += ch;
                } else {
                    expression += (" " + ch + " ");
                }
                this.getView().getModel("inputModel").setProperty("/expression", expression);
            },
            onListItemPress: function (oEvent) {
                var text = oEvent.getSource().getTitle();
                var expression = this.getView().getModel("inputModel").getProperty("/expression");
                expression += (" " + text + " ");
                this.getView().getModel("inputModel").setProperty("/expression", expression);
                /* MessageToast.show("Pressed : " + oEvent.getSource().getTitle()); */
            },
            onSaveInput() {
                var oM_ID = this.getView().byId("M_IDid").getValue();
                var oPath = "/ZTHDB002_SRV('" + oM_ID + "')";
                var oTable = this.getView().byId("table");
                for (var i = 0; i < oTable.getItems().length; i++) {
                    if (oTable.getItems()[i].getBindingContext().sPath === oPath) {
                        oTable.setSelectedItem(oTable.getItems()[i]);
                        var oDeleteContext = oTable.getSelectedItem().getBindingContext();
                        oDeleteContext.delete("$auto").then(function () {
                            // sales order successfully deleted
                            var oExpression = this.getView().byId("expressionid").getValue();
                            var oCreateContext = oTable.getBinding("items").create({
                                M_ID: this.getView().byId("M_IDid").getValue(),
                                L_ID: this.getView().byId("L_IDid").getValue(),
                                DESCRIPTION_ZF: this.getView().byId("DESCRIPTION_ZFid").getValue(),
                                DESCRIPTION_EN: this.getView().byId("DESCRIPTION_ENid").getValue(),
                                FORMAT: oExpression,
                                ACTIVE: this.getView().byId("ACTIVEid").getValue()
                            });

                            this.getView().byId("FORMATid").setValue(oExpression);
                            MessageBox.success("格式欄位已經更新");

                        }.bind(this), function (oError) {
                            // do error handling

                        });

                        break;
                    }
                }

            }
        });
    });
